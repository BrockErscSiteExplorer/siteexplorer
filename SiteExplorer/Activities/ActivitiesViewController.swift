//
//  ActivitiesViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-23.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

struct AnalysisRecord {
    var rock: Rock
    var analysis: Analysis
}

class ActivitiesViewController: UIViewController, ActivityTableCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var availableEnergyLabel: UILabel!
    @IBOutlet weak var availableTimeLabel: UILabel!
    @IBOutlet weak var availableTimeIcon: UIImageView!
    @IBOutlet weak var availableEnergyIcon: UIImageView!
    
    let rocksCollected = GameManager.sharedInstance.backpack.rocksCollected
    var sectionsExpanded: [Bool] = []
    var toAnalyze: [AnalysisRecord] = []
    var availableEnergy: Int = GameManager.sharedInstance.energy
    var availableTime: Int = GameManager.sharedInstance.time
    var totalEnergyCost: Int = 0
    var totalTimeCost: Int = 0

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initAvailableTimeEnergy()
        setTimeEnergyLabels()
    }
    
    @IBAction func onPerformAnalysisTouched(_ sender: Any?) {
        let cost = TimeEnergyCost(energy: totalEnergyCost, time: totalTimeCost)
        if (cost.energy <= availableEnergy && cost.time <= availableTime) {
            presentResultsViewController()
        } else {
            showNotEnoughResourcesAlert()
        }
    }
    
    private func presentResultsViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
        controller.setAnalysisRecords(analysisRecords: toAnalyze)
        self.present(controller, animated: true, completion: nil)
    }
    
    private func showNotEnoughResourcesAlert() {
        let title = "Not enough resources"
        
        let alert = UIAlertController(title: title, message: "You don't have enough resources to perform the analysis. Unselect some analysis and try again", preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! ResultsViewController
        controller.setAnalysisRecords(analysisRecords: toAnalyze)
        
        // Update the time and energy
        let cost = TimeEnergyCost(energy: totalEnergyCost, time: totalTimeCost)
        GameManager.sharedInstance.incurTimeEnergyCost(timeEnergyCost: cost)
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
    private func initAvailableTimeEnergy() {
        availableTimeIcon.image = #imageLiteral(resourceName: "time_ico").withRenderingMode(.alwaysTemplate)
        availableEnergyIcon.image = #imageLiteral(resourceName: "energy_ico").withRenderingMode(.alwaysTemplate)
        availableTimeIcon.tintColor = Constants.ENERGY_LABEL_COLOR
        availableTimeLabel.textColor = Constants.ENERGY_LABEL_COLOR
        availableEnergyIcon.tintColor = Constants.TIME_LABEL_COLOR
        availableEnergyLabel.textColor = Constants.TIME_LABEL_COLOR
    }
    
    private func setTimeEnergyLabels() {
        let availableEnergyDisplay = availableEnergy - totalEnergyCost
        let availableTimeDisplay = availableTime - totalTimeCost
        
        availableEnergyLabel.text = "\(availableEnergyDisplay <= 0 ? 0 : availableEnergyDisplay)"
        availableTimeLabel.text = "\(availableTimeDisplay <= 0 ? 0 : availableTimeDisplay)"
    }
    
    func onAnalyzeButtonTap(cell: ActivitiesTableViewCell) {
        if let index =  toAnalyze.index(where: {$0.rock == cell.rock && $0.analysis.title == cell.analysis.title }) {
            toAnalyze.remove(at: index)
            cell.setToSelectButton()
        } else {
            toAnalyze.append(AnalysisRecord(rock: cell.rock, analysis: cell.analysis))
            cell.setToRemoveButton()
        }
        
        updateCosts()
        setTimeEnergyLabels()
    }
    
    private func updateCosts() {
        var energyCost = 0
        var timeCost = 0
        
        for toPerform in toAnalyze {
            energyCost += toPerform.analysis.cost.energy
            timeCost += toPerform.analysis.cost.time
        }
        
        self.totalEnergyCost = energyCost
        self.totalTimeCost = timeCost
    }
    
    
}

extension ActivitiesViewController: UITableViewDelegate, UITableViewDataSource, ExpandableHeaderViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnalysisCell") as! ActivitiesTableViewCell
        cell.selectionStyle = .none
        let rock = rocksCollected[indexPath.section]
        let analysis = rock.analysis[indexPath.row]
        cell.setAnalysis(analysis: analysis)
        cell.setRock(rock: rock)
        cell.setAnalaysisLabel(text: analysis.title)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rocksCollected[section].analysis.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        for _ in 0..<rocksCollected.count {
            sectionsExpanded.append(true)
        }
        
        return rocksCollected.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ExpandableHeaderView()
        headerView.setTitle(title: "\(rocksCollected[section].type) rock")
        headerView.setSection(section: section)
        headerView.setDelegate(delegate: self)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (sectionsExpanded[indexPath.section]) {
            return 129
        }
        
        return 0
    }
    
    func onHeaderTouch(header: ExpandableHeaderView, section: Int) {
        sectionsExpanded[section] = !sectionsExpanded[section]
        
        // Reload the table, row by row
        tableView.beginUpdates()
        
        for i in 0..<rocksCollected[section].analysis.count {
            tableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        
        tableView.endUpdates()
    }
}
