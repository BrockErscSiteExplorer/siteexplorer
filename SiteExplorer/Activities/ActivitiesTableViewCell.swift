//
//  ActivitiesTabelViewCell.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-23.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

protocol ActivityTableCellDelegate {
    func onAnalyzeButtonTap(cell: ActivitiesTableViewCell)
}

class ActivitiesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var analysisLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeIcon: UIImageView!
    @IBOutlet weak var analyzeButton: RoundButton!
    @IBOutlet weak var energyLabel: UILabel!
    @IBOutlet weak var energyIcon: UIImageView!
    
    var delegate: ActivityTableCellDelegate!
    var analysis: Analysis!
    var rock: Rock!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setCostIconsAndLabels()
    }
    
    private func setCostIconsAndLabels() {
        timeIcon.image = #imageLiteral(resourceName: "time_ico").withRenderingMode(.alwaysTemplate)
        energyIcon.image = #imageLiteral(resourceName: "energy_ico").withRenderingMode(.alwaysTemplate)
        timeIcon.tintColor = .orange
        timeLabel.textColor = .orange
        energyIcon.tintColor = .yellow
        energyLabel.textColor = .yellow
    }
    
    func setDelegate(delegate: ActivityTableCellDelegate) {
        self.delegate = delegate
    }
    
    func setToSelectButton() {
        analyzeButton.backgroundColor = UIColor(cgColor: Constants.GREEN)
        setAnalyzeButtonText(text: "Select")
    }
    
    func setToRemoveButton() {
        analyzeButton.backgroundColor = Constants.RED
        setAnalyzeButtonText(text: "Remove")
    }
    
    func setAnalaysisLabel(text: String) {
        analysisLabel.text = text
    }
    
    func setRock(rock: Rock) {
        self.rock = rock
    }
    
    func setAnalysis(analysis: Analysis) {
        self.analysis = analysis
        energyLabel.text = "\(analysis.cost.energy)"
        timeLabel.text = "\(analysis.cost.time)"
    }
    
    func setAnalyzeButtonText(text: String) {
        analyzeButton.setTitle(text, for: .normal)
    }
    
    @IBAction func onAnalyzeButtonTap(_ sender: Any) {
        delegate?.onAnalyzeButtonTap(cell: self)
    }
}
