//
//  StartViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-27.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit
import Pages

class StartViewController: UIViewController {
    
    @IBAction func onStartPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if (GameManager.sharedInstance.hasSeenTutorial) {
            let map = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.present(map, animated: true, completion: nil)
        } else {
            let welcome = storyboard.instantiateViewController(withIdentifier: "IntroWelcome")
            let map = storyboard.instantiateViewController(withIdentifier: "IntroMap")
            let resources = storyboard.instantiateViewController(withIdentifier: "IntroResources")
            let problem = storyboard.instantiateViewController(withIdentifier: "IntroProblem")
            let finish = storyboard.instantiateViewController(withIdentifier: "IntroFinish")
            let log = storyboard.instantiateViewController(withIdentifier: "IntroLog")
            
            let controller = PagesController([welcome, map, log, problem, resources, finish])
            GameManager.sharedInstance.hasSeenTutorial = true
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    
}
