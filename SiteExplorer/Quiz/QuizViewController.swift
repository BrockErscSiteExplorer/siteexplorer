//
//  QuizViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class QuizViewController: UIViewController, QuizTableViewCellDelegate {
    @IBOutlet weak var category1Label: UILabel!
    @IBOutlet weak var category2Label: UILabel!
    @IBOutlet weak var problemTitleLabel: UILabel!
    @IBOutlet weak var userAnswerLabel: UILabel!
    
    var sites: [Site] = GameManager.sharedInstance.level.sites
    let problem = GameManager.sharedInstance.level.problem
    var category1: String!
    var category2: String!
    
    var userAnswer: String!
    var category1Count: Int = 0 {
        didSet {
            updateAnswerLabel()
        }
    }
    
    var category2Count: Int = 0 {
        didSet {
            updateAnswerLabel()
        }
    }
    
    override func viewDidLoad() {
        setCategories()
        setQuizLabels()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scoreViewController = segue.destination as? ScoreViewController {
            scoreViewController.setAnswer(hadCorrectAnswer: problem.answer == userAnswer)
        }
    }
    
    func setCategories() {
        category1 = problem.categories[0]
        category2 = problem.categories[1]
    }
    
    func setQuizLabels() {
        category1Label.text = category1
        category2Label.text = category2
        problemTitleLabel.text = "What evidence does each site suggest? " + problem.title
        updateAnswerLabel()
    }
    
    func updateCategory1Count(increment: Bool) {
        if (increment) {
            category1Count += 1
        } else {
            category1Count -= 1
        }
    }
    
    func updateCategory2Count(increment: Bool) {
        if (increment) {
            category2Count += 1
        } else {
            category2Count -= 1
        }
    }
    
    func updateAnswerLabel() {
        if (category1Count == category2Count) {
            self.userAnswer = "Undetermined"
        } else if (category1Count > category2Count) {
            self.userAnswer = category1
        } else {
            self.userAnswer = category2
        }
        
        userAnswerLabel.text = userAnswer
    }

}

extension QuizViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sites.count
    }
    
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuizCell") as! QuizTableViewCell
        cell.setSiteTitle(title: sites[indexPath.row].title)
        cell.delegate = self
        return cell
    }
}
