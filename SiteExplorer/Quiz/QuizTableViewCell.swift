//
//  QuizTableViewCell.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

protocol QuizTableViewCellDelegate {
    func updateCategory1Count(increment: Bool)
    func updateCategory2Count(increment: Bool)
}

class QuizTableViewCell: UITableViewCell {
    @IBOutlet weak var siteTitleLabel: UILabel!
    
    var delegate: QuizTableViewCellDelegate!
    
    func setSiteTitle(title: String) {
        siteTitleLabel.text = title
    }
    
    @IBAction func category1Changed(_ sender: UISwitch) {
        if (sender.isOn) {
            delegate.updateCategory1Count(increment: true)
        } else {
            delegate.updateCategory1Count(increment: false)
        }
    }
    
    @IBAction func category2Changed(_ sender: UISwitch) {
        if (sender.isOn) {
            delegate.updateCategory2Count(increment: true)
        } else {
            delegate.updateCategory2Count(increment: false)
        }
    }
    
}
