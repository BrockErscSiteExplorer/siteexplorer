//
//  LogViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class LogViewController: UIViewController {
    
    @IBAction func onBackTouched(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var records: [LogRecord]! = GameManager.sharedInstance.log.records
}

extension LogViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogCell") as! LogTableViewCell
        cell.setLogRecord(logRecord: records[indexPath.row])
        return cell
    }
}
