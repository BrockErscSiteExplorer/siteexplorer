//
//  LogTableViewCell.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class LogTableViewCell: UITableViewCell {
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var logLabel: UILabel!
    var logRecord: LogRecord!
    
    func setLogRecord(logRecord: LogRecord) {
        self.logRecord = logRecord
        self.logLabel.text = "\(logRecord.site.title): \(logRecord.result.hint)"
        self.resultImage.image = logRecord.result.resultImage
    }
}
