//
//  IconLabelView.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-14.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class IconLabelView: UIView {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet var view: UIView!
    @IBOutlet weak var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("IconLabelView", owner: self, options: nil)
    }
    
    public func setIcon(icon: UIImage) {
        iconView.image = icon
    }
    
    public func setLabelText(text: String) {
        label.text = text
    }
    
    public func setIconAndTextColor(color: UIColor) {
        iconView.tintColor = color
        label.textColor = color
    }
    
}
