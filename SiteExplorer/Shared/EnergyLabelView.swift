//
//  EnergyLabelView.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-14.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class EnergyLabelView: IconLabelView, TimeEnergyObserver {
    var gameManager: GameManager!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        gameManager = GameManager.sharedInstance
        setIcon(icon: #imageLiteral(resourceName: "energy_ico").withRenderingMode(.alwaysTemplate))
        setIconAndTextColor(color: Constants.ENERGY_LABEL_COLOR)
        gameManager.attachTimeEnergyObserver(observer: self)
        setLabelText(text: "\(gameManager.energy!)")
        self.addSubview(view)
    }
    
    func update() {
        setLabelText(text: "\(gameManager.energy!)")
    }
}
