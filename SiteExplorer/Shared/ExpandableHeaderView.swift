//
//  ExpandableHeaderView.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-24.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

protocol ExpandableHeaderViewDelegate {
    func onHeaderTouch(header: ExpandableHeaderView, section: Int);
}

class ExpandableHeaderView: UITableViewHeaderFooterView {
    var delegate: ExpandableHeaderViewDelegate!
    var section: Int!
    
    func setSection(section: Int) {
        self.section = section
    }
    
    func setDelegate(delegate: ExpandableHeaderViewDelegate) {
        self.delegate = delegate
    }
    
    func setTitle(title: String) {
        self.textLabel!.text = title
    }
    
    func onHeaderTouch(gestureRecognizer: UITapGestureRecognizer) {
        let header = gestureRecognizer.view as! ExpandableHeaderView
        delegate?.onHeaderTouch(header: header, section: header.section)
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onHeaderTouch)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.textLabel?.textColor = .white
    }
}
