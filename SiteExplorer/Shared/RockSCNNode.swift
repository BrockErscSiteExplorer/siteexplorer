//
//  RockSCNNode.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-08.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import SceneKit

struct Position {
    var x: Float
    var y: Float
    var z: Float
}

class RockSCNNode: SCNNode {

    var rock: Rock = Rock()
    
    init(rock: Rock, position: Position) {
        super.init()
        self.geometry = SCNSphere(radius: 0.5)
        self.position = SCNVector3(position.x, position.y, position.z)
        self.opacity = 0.2
        self.rock = rock
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func hide() {
        SCNTransaction.begin()
        self.opacity = 0
        self.isHidden = true
        SCNTransaction.commit()
    }
    
    func collect() {
        rock.collect()
    }
    
}
