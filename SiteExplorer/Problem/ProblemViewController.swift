//
//  ProblemViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class ProblemViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var problemImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var problem: Problem = GameManager.sharedInstance.level.problem
    
    override func viewDidLoad() {
        titleLabel.text = problem.title
        descriptionTextView.text = problem.desc
        problemImageView.image = problem.image
    }
}
