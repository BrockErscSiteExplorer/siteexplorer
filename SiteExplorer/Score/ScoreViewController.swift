//
//  ScoreViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class ScoreViewController: UIViewController {
    
    // Score labels
    @IBOutlet weak var correctAnswerScoreLabel: UILabel!
    @IBOutlet weak var energyBonusLabel: UILabel!
    @IBOutlet weak var timeBonusLabel: UILabel!
    @IBOutlet weak var itemsCollectedBonusLabel: UILabel!
    @IBOutlet weak var finalScoreLabel: UILabel!
    
    // Icons
    @IBOutlet weak var energyIcon: UIImageView!
    @IBOutlet weak var timeIcon: UIImageView!
    
    var hadCorrectAnswer: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setIconColors()
        setScoreLabels()
    }
    
    func setAnswer(hadCorrectAnswer: Bool) {
        self.hadCorrectAnswer = hadCorrectAnswer
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        GameManager.sharedInstance.resetAll()
    }
    
    func setIconColors() {
        energyIcon.image = #imageLiteral(resourceName: "energy_ico").withRenderingMode(.alwaysTemplate)
        energyIcon.tintColor = Constants.ENERGY_LABEL_COLOR
        
        timeIcon.image = #imageLiteral(resourceName: "time_ico").withRenderingMode(.alwaysTemplate)
        timeIcon.tintColor = Constants.TIME_LABEL_COLOR
    }
    
    func setScoreLabels() {
        var finalScore = 0
        let gameManager = GameManager.sharedInstance
        var correctAnswerBonus = 0
        
        if (hadCorrectAnswer) {
            correctAnswerBonus += 10000
            correctAnswerScoreLabel.textColor = .green
        } else {
            correctAnswerScoreLabel.textColor = .red
        }
        
        let availableEnergyScore = gameManager.energy * 10
        let availableTimeScore = gameManager.time * 10
        let itemsCollectedBonus = gameManager.backpack.rocksCollected.count * 10
        
        finalScore += correctAnswerBonus + availableTimeScore + availableEnergyScore + itemsCollectedBonus
        
        correctAnswerScoreLabel.text = "\(correctAnswerBonus)"
        energyBonusLabel.text = "\(availableEnergyScore)"
        timeBonusLabel.text = "\(availableTimeScore)"
        itemsCollectedBonusLabel.text = "\(itemsCollectedBonus)"
        finalScoreLabel.text = "\(finalScore)"
        
    }
}
