//
//  SiteAnnotation.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//

import UIKit
import MapKit

protocol SiteCalloutViewDelegate: class {
    func onExploreTouched(site: Site)
}

class SiteCalloutView: UIView {
    
    @IBOutlet weak var siteTitle: UILabel!
    @IBOutlet weak var siteDesc: UITextView!
    @IBOutlet weak var explore: UIButton!
    
    var site: Site!
    
    weak var delgate: SiteCalloutViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 3
    }
    
    @IBAction func onExploreTouched(_ sender: Any) {
        delgate?.onExploreTouched(site: site)
    }
    
    // MARK: Hit test, consume touch interactions for custom callout view
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let result = explore.hitTest(convert(point, to: explore), with: event) {
            return result
        }
        
        if let result = siteDesc.hitTest(convert(point, to: siteDesc), with: event) {
            return result
        }
        
        if let result = siteTitle.hitTest(convert(point, to: siteTitle), with: event) {
            return result
        }
        
        return nil
    }
    
    func configureWithSite(site: Site) {
        self.site = site
        siteTitle.text = site.title
        siteDesc.text = site.desc
    }
}
