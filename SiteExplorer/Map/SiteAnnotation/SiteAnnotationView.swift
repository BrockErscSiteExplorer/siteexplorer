//
//  SiteAnnotationView.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//
import Foundation
import MapKit
import Pulsator

protocol SiteSelectedDelegate: class {
    func siteSelected(site: Site)
}

class SiteAnnotationView: MKAnnotationView {
    weak var customCalloutView: SiteCalloutView?
    weak var siteSelectedDelegate: SiteSelectedDelegate?
    var site: Site!
    
    override var annotation: MKAnnotation? {
        willSet {
            customCalloutView?.removeFromSuperview()
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        self.site = (annotation as! SiteAnnotation).site
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.canShowCallout = false
        self.image = #imageLiteral(resourceName: "Unlock_Tapped_Site")
        setPulse()
    }
    
    func setPulse() {
        if (site.isCurrentSite) {
            self.layer.opacity = 1
            showPulse()
        } else {
            self.layer.opacity = 0.4
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.canShowCallout = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        
        if selected && site.isCurrentSite {
            if siteSelectedDelegate != nil {
                siteSelectedDelegate?.siteSelected(site: site)
            }
            
            // remove old custom callout (if any)
            self.customCalloutView?.removeFromSuperview()
            
            if let newCustomCalloutView = loadSiteCalloutView() {
                // fix location from top-left to its right place.
                newCustomCalloutView.frame.origin.x -= newCustomCalloutView.frame.width / 2.0 - (self.frame.width / 2.0)
                newCustomCalloutView.frame.origin.y -= newCustomCalloutView.frame.height + 10
                
                // set custom callout view
                self.addSubview(newCustomCalloutView)
                self.customCalloutView = newCustomCalloutView
            }
        } else {
            if customCalloutView != nil {
                self.customCalloutView!.removeFromSuperview()
            }
        }
    }
    
    func showPulse() {
        let pulsator = Pulsator()
        pulsator.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        pulsator.numPulse = 1
        pulsator.radius = 60
        pulsator.animationDuration = 3
        pulsator.backgroundColor = Constants.PULSE_COLOR
        layer.addSublayer(pulsator)
        pulsator.start()
    }
    
    func loadSiteCalloutView() -> SiteCalloutView? {
        if let views = Bundle.main.loadNibNamed("SiteCalloutView", owner: self, options: nil) as? [SiteCalloutView], views.count > 0 {
            let siteCalloutView = views.first!
            
            if let siteCalloutViewDelegate = self.siteSelectedDelegate as? SiteCalloutViewDelegate {
                siteCalloutView.delgate = siteCalloutViewDelegate
            }
            
            siteCalloutView.configureWithSite(site: site)
            return siteCalloutView
        }
        
        return nil
    }
    
    // MARK: Hit test, consume touches for custom annotation/marker
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let parentHitView = super.hitTest(point, with: event) {
            return parentHitView
        } else {
            guard customCalloutView != nil else {
                return nil
            }
            
            return customCalloutView!.hitTest(convert(point, to: customCalloutView!), with: event)
        }
    }
    
}
