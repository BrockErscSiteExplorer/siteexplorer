//
//  SiteAnnotation.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import MapKit

class SiteAnnotation: NSObject, MKAnnotation {
    
    var site: Site
    var coordinate: CLLocationCoordinate2D
    
    init(site: Site) {
        self.site = site
        self.coordinate = site.coordinate
    }
    
    var title: String? {
        return site.title
    }
    
    var desc: String? {
        return site.desc
    }
}
