//  
//  MapViewController.swift
//  View controller for the map screen
//  SiteExplorer
//
//  Created by Fahad on 2017-08-03.
//  Copyright © 2017 Fahad. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, SiteSelectedDelegate, SiteCalloutViewDelegate, UIApplicationDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var span: MKCoordinateSpan!
    
    var askToMoveOn: Bool = false
    var level: Level!
    // Camera options
    let latDelta: CLLocationDegrees = 0.0125
    let lonDelta: CLLocationDegrees = 0.0125
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initMap() {
        mapView.isZoomEnabled = false;
        mapView.isScrollEnabled = false;
        mapView.mapType = .satellite
    }
    
    func loadSites() {
        
        DispatchQueue.main.async() {
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        
        
        for site in level.sites {
            if (site.isCurrentSite) {
                setMapRegionToSite(site: site)
            }
            
            let sitePoint = SiteAnnotation(site: site)
            
            DispatchQueue.main.async {
                self.mapView.addAnnotation(sitePoint)
            }
        }
    }
    
    func saveCurrentSiteResults(results: [Result]) {
        let log = GameManager.sharedInstance.log
        let currentSite = GameManager.sharedInstance.level.getCurrentSite()
        
        for result in results {
            let record = LogRecord(site: currentSite, result: result)
            log?.addRecord(record: record)
        }
        
        GameManager.sharedInstance.updateLog()
    }
    
    func shouldAskToMoveOn() {
        askToMoveOn = true
    }
    
    func showMoveOn() {
        let travelCost = GameManager.sharedInstance.level.getNextSite().travelCost
        let title = "Move on to next Site? This will cost you \(travelCost.energy) energy and \(travelCost.time) time"
        
        let alert = UIAlertController(title: title, message: "Would you like to move on to the next site?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: handleMoveOn))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRecharge() {
        let title = "You ran out energy, recharge?"
        
        let alert = UIAlertController(title: title, message: "You ran out of energy, would you like to recharge in exchange for 10 time?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Recharge", style: UIAlertActionStyle.cancel, handler: handleRecharge))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNoResources() {
        let title = "You ran out of resources, answer the question!"
        
        let alert = UIAlertController(title: title, message: "You ran out of resources! It's time to solve the problem!", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK, I'm ready", style: UIAlertActionStyle.cancel, handler: handleNoResourcesOK))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleRecharge(action: UIAlertAction) {
        GameManager.sharedInstance.energy = 100
        GameManager.sharedInstance.incurTimeEnergyCost(timeEnergyCost: TimeEnergyCost(energy: 0, time: 10))
    }
    
    func handleNoResourcesOK(action: UIAlertAction) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ProblemViewController") as! ProblemViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func handleMoveOn(action: UIAlertAction) {
        level.moveToNextSite()
        loadSites()
        askToMoveOn = false
    }
    
    func setMapRegionToSite(site: Site) {
        let location: CLLocationCoordinate2D = site.coordinate
        let region: MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)
    }
    
    // MARK: MapViewDelegate
    // Used to override annotation views
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let annotationView = SiteAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
        
        let siteAnnotationView = annotationView 
        
        siteAnnotationView.siteSelectedDelegate = self
        
        return annotationView
    }
    
    // MARK: SiteSelectedDelegate, 
    // Called when the site is selected by the user on the map
    
    func siteSelected(site: Site) {
        setMapRegionToSite(site: site)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        level = GameManager.sharedInstance.level
        mapView.delegate = self
        span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        initMap()
        loadSites()
        
        let energy = GameManager.sharedInstance.energy
        let time = GameManager.sharedInstance.time
        
        if (energy == 0 && time! >= 10) {
            showRecharge()
        } else if (time == 0 || (energy == 0 && time == 0)) {
            showNoResources()
            return
        }
        
        if (askToMoveOn) {
            showMoveOn()
        }
    }
    
    
    // MARK: SiteCalloutViewDelegate
    // Called when the explore button is touched in the callout
    // Launches the panorama view
    
    func onExploreTouched(site: Site) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PanoramaViewController") as! PanoramaViewController
        controller.configureWithSite(site: site)
        self.present(controller, animated: true, completion: nil)
    }

}
