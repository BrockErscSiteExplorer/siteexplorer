//
//  PanoramaViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-06.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit
import CTPanoramaView
import SceneKit
import Toaster

class PanoramaViewController: UIViewController, ChildNodeTouchDelegate {
    
    @IBOutlet weak var panoramaView: CTPanoramaView!
    var site: Site!
    var gameManager: GameManager!
    
    override func viewDidLoad() {
        self.gameManager = GameManager.sharedInstance
        panoramaView.panoramaType = .cylindrical
        panoramaView.delegate = self
        panoramaView.childNodes = site.rocks
        panoramaView.image = UIImage(named: "mars")
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
    func configureWithSite(site: Site) {
        self.site = site
    }
    
    func onTouch(child: SCNNode) {
        if let rockNode = child as? RockSCNNode {
            Toast(text: "Collected a \(rockNode.rock.type) rock!").show()
            site.collectRock(rock: rockNode)
        }
    }
    
}
