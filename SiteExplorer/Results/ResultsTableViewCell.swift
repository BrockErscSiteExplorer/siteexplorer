//
//  ResultsTableViewCell.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-25.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var analysisLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var resultImage: UIImageView!

    var result: Result!
    var analysisRecord: AnalysisRecord!
    
    func setAnalysisRecord(record: AnalysisRecord) {
        self.analysisRecord = record
        self.result = record.rock.analyze(analysis: record.analysis)
        self.hintLabel.text = "Hint: " + result.hint
        self.analysisLabel.text = "\(analysisRecord.rock.type): \(analysisRecord.analysis.title) analysis"
        self.resultImage.image = result.resultImage
    }

}
