//
//  ResultsViewController.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-25.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class ResultsViewController: UIViewController {
    var analysisRecords: [AnalysisRecord] = []
    var results: [Result] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        GameManager.sharedInstance.backpack.emptyBackpack()
        let mapViewController = segue.destination as! MapViewController
        mapViewController.saveCurrentSiteResults(results: results)
        mapViewController.shouldAskToMoveOn()
    }
    
    func setAnalysisRecords(analysisRecords: [AnalysisRecord]) {
        self.analysisRecords = analysisRecords
    }
}

extension ResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return analysisRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell") as! ResultsTableViewCell
        let record = analysisRecords[indexPath.row]
        cell.setAnalysisRecord(record: record)
        results.append(cell.result)
        return cell
    }
}
