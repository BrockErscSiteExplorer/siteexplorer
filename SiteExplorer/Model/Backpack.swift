//
//  Backpack.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-15.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
class Backpack: NSObject, NSCoding {
    var rocksCollected: [Rock] = [] {
        willSet {
            GameManager.sharedInstance.updateBackpack()
        }
    }

    override init() {
        super.init()
    }
    
    func collect(rock: Rock) {
        rocksCollected.append(rock)
        GameManager.sharedInstance.updateBackpack()
    }
    
    func rockCount() -> Int {
        return rocksCollected.count
    }
    
    func emptyBackpack() {
        rocksCollected.removeAll()
        GameManager.sharedInstance.updateBackpack()
    }
    
    // MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(rocksCollected, forKey: "rocksCollected")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.rocksCollected = aDecoder.decodeObject(forKey: "rocksCollected") as! [Rock]
    }
    
}
