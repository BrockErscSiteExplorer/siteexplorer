//
//  Site.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import MapKit

struct Coordinate {
    var lat: Double
    var long: Double
}

class Site: NSObject, NSCoding {
    var isCurrentSite: Bool
    var title: String
    var desc: String
    var coordinate: CLLocationCoordinate2D
    var rocks: [RockSCNNode]
    var travelCost: TimeEnergyCost
    
    init(title: String, desc: String, coord: Coordinate, rocks: [RockSCNNode]) {
        self.isCurrentSite = false
        self.title = title
        self.desc = desc
        self.coordinate = CLLocationCoordinate2D(latitude: coord.lat, longitude: coord.long)
        self.rocks = rocks
        self.travelCost = TimeEnergyCost(energy: 10, time: 10)
    }
    
    func removeRock(rock: RockSCNNode) {
        if let index = rocks.index(of: rock) {
            rocks.remove(at: index)
        }
    }
    
    func travelToSite() {
        GameManager.sharedInstance.incurTimeEnergyCost(timeEnergyCost: travelCost)
        GameManager.sharedInstance.level.setToCurrentSite(site: self)
    }
    
    func collectRock(rock: RockSCNNode) {
        removeRock(rock: rock)
        rock.hide()
        rock.collect()
        GameManager.sharedInstance.updateLevel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.isCurrentSite = aDecoder.decodeBool(forKey: "current")
        self.title = (aDecoder.decodeObject(forKey: "title") as? String)!
        self.desc = (aDecoder.decodeObject(forKey: "desc") as? String)!
        let lat = aDecoder.decodeDouble(forKey: "lat")
        let long = aDecoder.decodeDouble(forKey: "long")
        self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.rocks = (aDecoder.decodeObject(forKey: "rocks") as? [RockSCNNode])!
        self.travelCost = ((aDecoder.decodeObject(forKey: "travelCost")) as? TimeEnergyCost)!
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.isCurrentSite, forKey: "current")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.desc, forKey: "desc")
        aCoder.encode(self.coordinate.latitude, forKey: "lat")
        aCoder.encode(self.coordinate.longitude, forKey: "long")
        aCoder.encode(self.rocks, forKey: "rocks")
        aCoder.encode(self.travelCost, forKey: "travelCost")
    }
    
}
