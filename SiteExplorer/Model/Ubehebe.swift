//
//  Ubehebe.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import MapKit

class Ubehebe: NSObject, NSCoding, Level {
    
    // MARK: Level protocol
    var problem: Problem = Problem(title: "Volcanic or Impact?", desc: "This crater was formed a very long time ago, and we are trying to figure out the cause of this massive crater. Maybe the rocks laying around here can help..", image: #imageLiteral(resourceName: "mars"), categories: ["Volcanic", "Impact"], answer: "Impact")
    
    var sites: [Site] = [
            Site(title: "UC1", desc: "Test", coord: Coordinate(lat: 37.005015, long: -117.452006),
                 rocks: [
                    RockSCNNode(rock: Rock(), position: Position(x: 10, y: 2, z: 0))
                ]
            ),
            Site(title: "UC2", desc: "Test", coord: Coordinate(lat: 37.003925, long: -117.451567),
                 rocks: [
                    RockSCNNode(rock: Rock(), position: Position(x: 0, y: 0, z: 0))
                ]
            ),
            Site(title: "UC3", desc: "Test", coord: Coordinate(lat: 37.00391, long: -117.451543),
                 rocks: [
                    RockSCNNode(rock: Rock(), position: Position(x: 0, y: 0, z: 0))
                ]
            ),
            Site(title: "UC6", desc: "Test", coord: Coordinate(lat: 37.010525, long: -117.454777),
                 rocks: [
                    RockSCNNode(rock: Rock(), position: Position(x: 10, y: 2, z: 0)),
                    RockSCNNode(rock: Rock(), position: Position(x: 8, y: 2, z: 4)),
                    RockSCNNode(rock: Rock(), position: Position(x: 10, y: 2, z: 5))
                ]
            ),
            Site(title: "UC7", desc: "Test", coord: Coordinate(lat: 37.00429, long: -117.451822),
                 rocks: [
                    RockSCNNode(rock: Rock(), position: Position(x: 0, y: 0, z: 0))
                ]
            )
    ]
    
    var title: String {
        return "Ubehebe"
    }
    
    var levelTime: Int {
        return 60 * 5
    }
    
    func getStartingSite() -> Site {
        return self.sites[2]
    }

    override init() {
        super.init()
    }
    
    // MARK: NSCoding
    
    required init(coder decoder: NSCoder) {
        if let loadedSites = decoder.decodeObject(forKey: "sites") as? [Site] {
            self.sites = loadedSites
        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(sites, forKey: "sites")
    }
}
