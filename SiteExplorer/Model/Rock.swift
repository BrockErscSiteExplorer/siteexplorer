//
//  Rock.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-15.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation

enum RockType {
    case Igneous
    case Metamorphic
    case Sedimentary
    
    static func getTypeFromHashValue(hashValue: Int) -> RockType {
        switch hashValue {
        case 0:
            return .Igneous
        case 1:
            return .Metamorphic
        case 2:
            return .Sedimentary
        default:
            return .Igneous
        }
    }
}

class Rock: NSObject, NSCoding, Analyzable {
    
    var type: RockType
    var result: Result
    var subjectTitle: String
    var analysis: [Analysis] = [MicroimageAnalysis(), GeochemicalAnalysis()]
    
    override init() {
        self.result = Result()
        self.type = .Igneous
        self.subjectTitle = "\(self.type) Rock"
        super.init()
        initRockTypeRandomly()
    }
    
    func initRockTypeRandomly() {
        let randomNum = arc4random_uniform(9)
        if (randomNum < 3) {
            self.type = .Igneous
        } else if (randomNum >= 3 && randomNum < 6) {
            self.type = .Metamorphic
        } else {
            self.type = .Sedimentary
        }
    }
    
    func collect() {
        GameManager.sharedInstance.backpack.collect(rock: self)
    }
    
    func analyze(analysis: Analysis) -> Result {
        return analysis.generateResult(analyzable: self)
    }

    required init?(coder aDecoder: NSCoder) {
        self.result = aDecoder.decodeObject(forKey: "result") as! Result
        let hashValue = aDecoder.decodeInteger(forKey: "type")
        self.type = RockType.getTypeFromHashValue(hashValue: hashValue)
        self.subjectTitle = "\(self.type) Rock"
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.result, forKey: "result")
        aCoder.encode(self.type.hashValue, forKey: "type")
    }

}
