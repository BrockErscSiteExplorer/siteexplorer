//
//  ResultsGenerator.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-22.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class ResultsGenerator {
    
    private static let hints = [
        "This rock has black vesicles",
        "This rock has dark rims around it",
        "This rock is composed mainly of plagioclase and pyroxene minerals"
    ]
    
    private static let microscopicImages: [UIImage] = [
        #imageLiteral(resourceName: "microscopic_1"),
        #imageLiteral(resourceName: "microscopic_2")
    ]
    
    private static let geochemImages: [UIImage] = [
        #imageLiteral(resourceName: "geochemistry_1"),
        #imageLiteral(resourceName: "geochemistry_2")
    ]
    
    
    static func generateMicroscopicResult(for analyzable : Analyzable) -> Result {
        let hintIndex = ResultsGenerator.getRandomIndexForList(list: ResultsGenerator.hints)
        let microIndex = ResultsGenerator.getRandomIndexForList(list: ResultsGenerator.microscopicImages)
        let hint = ResultsGenerator.hints[hintIndex]
        let image = ResultsGenerator.microscopicImages[microIndex]
        let label = "\(analyzable.subjectTitle): Microimage Analysis"
        return Result(label: label, hint: hint, resultImage: image)
    }
    
    static func generateGeochemResult(for analyzable : Analyzable) -> Result {
        let hintIndex = ResultsGenerator.getRandomIndexForList(list: ResultsGenerator.hints)
        let microIndex = ResultsGenerator.getRandomIndexForList(list: ResultsGenerator.microscopicImages)
        let hint = ResultsGenerator.hints[hintIndex]
        let image = ResultsGenerator.geochemImages[microIndex]
        let label = "\(analyzable.subjectTitle): Geochemical Analysis"
        return Result(label: label, hint: hint, resultImage: image)
    }
    
    private static func getRandomIndexForList(list: [Any]) -> Int {
        return Int(arc4random_uniform(UInt32(list.count)))
    }
}
