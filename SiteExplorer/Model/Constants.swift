//
//  Options.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-13.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

let ACCENT_GREEN = UIColor(red: 0.698, green: 0.862, blue: 0.521, alpha: 1).cgColor
let WARNING_RED = UIColor.red

struct Constants {
    static let GREEN = ACCENT_GREEN
    static let RED = WARNING_RED
    static let PULSE_COLOR = ACCENT_GREEN
    static let TIME_LABEL_COLOR = UIColor.orange
    static let ENERGY_LABEL_COLOR = UIColor.yellow
}
