//
//  Analysis.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-24.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
protocol Analysis {
    var title: String { get }
    var cost: TimeEnergyCost { get }
    func generateResult(analyzable: Analyzable) -> Result
}
