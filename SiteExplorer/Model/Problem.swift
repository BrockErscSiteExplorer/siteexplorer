//
//  Problem.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-26.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class Problem: NSObject, NSCoding {
    
    var title: String!
    var desc: String!
    var image: UIImage!
    var categories: [String]!
    var answer: String!
    
    init(title: String, desc: String, image: UIImage, categories: [String], answer: String) {
        self.title = title
        self.desc = desc
        self.image = image
        self.categories = categories
        self.answer = answer
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.desc = aDecoder.decodeObject(forKey: "desc") as! String
        self.image = aDecoder.decodeObject(forKey: "image") as! UIImage
        self.categories = aDecoder.decodeObject(forKey: "categories") as! [String]
        self.answer = aDecoder.decodeObject(forKey: "answer") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(categories, forKey: "categories")
        aCoder.encode(answer, forKey: "answer")
        
    }
}
