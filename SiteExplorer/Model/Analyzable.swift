//
//  Analyzable.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-22.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
protocol Analyzable {
    var subjectTitle: String { get }
    var analysis: [Analysis] { get }
    func analyze(analysis: Analysis) -> Result
}
