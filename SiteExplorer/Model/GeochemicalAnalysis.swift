//
//  GeochemicalAnalysis.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-24.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
class GeochemicalAnalysis: Analysis {
    var title: String {
        return "Geochemical"
    }
    
    var cost: TimeEnergyCost {
        return TimeEnergyCost(energy: 5, time: 5)
    }
    
    func generateResult(analyzable: Analyzable) -> Result {
        return ResultsGenerator.generateGeochemResult(for: analyzable)
    }
}
