//
//  TimeEnergyCost.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-14.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
class TimeEnergyCost: NSObject, NSCoding {
    var timeKey = "time"
    var energyKey = "energy"
    
    var time: Int = 0
    var energy: Int = 0
    
    init(energy: Int, time: Int) {
        self.energy = energy
        self.time = time
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.time = aDecoder.decodeInteger(forKey: timeKey)
        self.energy = aDecoder.decodeInteger(forKey: energyKey)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(energy, forKey: energyKey)
        aCoder.encode(time, forKey: timeKey)
    }
    
}
