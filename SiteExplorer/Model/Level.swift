//
//  Level.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-05.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import MapKit

protocol Level {
    var title: String { get }
    var levelTime: Int { get }
    var problem: Problem { get }
    var sites: [Site] { get }
    func getStartingSite() -> Site
}

extension Level {
    func getCurrentSite() -> Site {
        for site in sites {
            if (site.isCurrentSite) {
                return site
            }
        }
        
        return getStartingSite()
    }
    
    func getNextSite() -> Site {
        let currentIndex = sites.index(of: getCurrentSite())
        let index = currentIndex! + 1
        return sites[index % sites.count]
    }
    
    func moveToNextSite() {
        let nextSite = getNextSite()
        nextSite.travelToSite()
        GameManager.sharedInstance.updateLevel()
    }
    
    
    func setToCurrentSite(site: Site) {
        resetCurrentSiteStatus()
        site.isCurrentSite = true
    }
    
    private func resetCurrentSiteStatus() {
        for site in sites {
            site.isCurrentSite = false
        }
    }
}
