//
//  MicroimageAnalysis.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-24.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
class MicroimageAnalysis: Analysis {
    var title: String {
        return "Microimage"
    }
    
    var cost: TimeEnergyCost {
        return TimeEnergyCost(energy: 3, time: 3)
    }
    
    func generateResult(analyzable: Analyzable) -> Result {
        return ResultsGenerator.generateMicroscopicResult(for: analyzable)
    }
}
