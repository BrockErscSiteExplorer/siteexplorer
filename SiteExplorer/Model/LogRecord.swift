//
//  LogRecord.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-25.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation

class LogRecord: NSObject, NSCoding {
    
    var site: Site!
    var result: Result!
    
    init(site: Site, result: Result) {
        self.site = site
        self.result = result
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.site = aDecoder.decodeObject(forKey: "site") as! Site
        self.result = aDecoder.decodeObject(forKey: "result") as! Result
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(site, forKey: "site")
        aCoder.encode(result, forKey: "result")
    }
}
