//
//  Log.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-25.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation

class Log: NSObject, NSCoding {
    var records: [LogRecord] = []
    
    override init() {
        super.init()
    }
    
    func addRecord(record: LogRecord) {
        records.append(record)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(records, forKey: "records")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.records = aDecoder.decodeObject(forKey: "records") as! [LogRecord]
    }
}
