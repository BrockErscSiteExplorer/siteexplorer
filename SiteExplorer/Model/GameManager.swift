//
//  GameManager.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-12.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation

protocol TimeEnergyObserver {
    func update()
}

class GameManager {
    
    static var sharedInstance: GameManager = GameManager()
    private let levelKey: String = "level"
    private let timeKey: String = "time"
    private let energyKey: String = "energy"
    private let backpackKey: String = "backpack"
    private let logKey: String = "log"
    private let tutorialKey: String = "tutorial"
    
    var level: Level! {
        didSet {
            updateLevel()
        }
    }
    
    var backpack: Backpack! {
        didSet {
            updateBackpack()
        }
    }
    
    var log: Log! {
        didSet {
            updateLog()
        }
    }
    
    // Data
    var time: Int! {
        willSet(time) {
            updateData(data: time, key: timeKey)
            updateTimeEnergyObservers()
        }
    }
    
    var energy: Int! {
        willSet(energy) {
            updateData(data: energy, key: energyKey)
            updateTimeEnergyObservers()
        }
    }
    
    var hasSeenTutorial: Bool = false {
        willSet(hasSeenTutorial) {
            updateData(data: hasSeenTutorial, key: "tutorial")
        }
    }
    
    var observers: [TimeEnergyObserver] = []
    
    private init() {
        let defaults = UserDefaults.standard
        
        do {
            if let levelData = loadArchivedData(key: levelKey) {
                try self.level = NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(levelData) as? Level!
            } else {
                self.level = Ubehebe()
                level.setToCurrentSite(site: level.getStartingSite())
                updateLevel()
            }
            
            if let backpackData = loadArchivedData(key: backpackKey) {
                try self.backpack = NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(backpackData) as? Backpack!
            } else {
                self.backpack = Backpack()
            }
            
            if let logData = loadArchivedData(key: logKey) {
                try self.log = NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(logData) as? Log!
            } else {
                self.log = Log()
            }
        } catch _ as NSError {
            fatalError("Couldn't load level or backpack!")
        }
        
        self.hasSeenTutorial = defaults.bool(forKey: tutorialKey)
        self.time = defaults.object(forKey: timeKey) as! Int! ?? 100
        self.energy = defaults.object(forKey: energyKey) as! Int! ?? 100
    }
    
    public func attachTimeEnergyObserver(observer: TimeEnergyObserver) {
        observers.append(observer)
    }
    
    public func incurTimeEnergyCost(timeEnergyCost: TimeEnergyCost) {
        if (self.time - timeEnergyCost.time >= 0 && self.energy - timeEnergyCost.energy >= 0) {
            self.time = time - timeEnergyCost.time
            self.energy = energy - timeEnergyCost.energy
        } else {
            self.time = 0
            self.energy = 0
        }
    }
    
    public func updateLog() {
        updateArchivedData(data: log, key: logKey)
    }
    
    public func updateBackpack() {
        updateArchivedData(data: backpack, key: backpackKey)
    }
    
    public func updateLevel() {
        updateArchivedData(data: level, key: levelKey)
    }
    
    private func updateArchivedData(data: Any, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(NSKeyedArchiver.archivedData(withRootObject: data), forKey: key)
        defaults.synchronize()
    }
    
    private func updateTimeEnergyObservers() {
        for observer in observers {
            observer.update()
        }
    }
    
    private func updateData(data: Any, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: key)
        defaults.synchronize()
    }
    
    
    func resetAll() {
        removeAllKeysFromDefaults()
        GameManager.sharedInstance = GameManager()
    }
    
    private func removeAllKeysFromDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        
        defaults.synchronize()
    }
    
    
    private func loadArchivedData(key: String) -> NSData? {
        let defaults = UserDefaults.standard
        guard let data = defaults.object(forKey: key) as? NSData else {
            return nil
        }
        
        return data
    }
}
