//
//  Result.swift
//  SiteExplorer
//
//  Created by Fahad on 2017-08-22.
//  Copyright © 2017 Fahad. All rights reserved.
//

import Foundation
import UIKit

class Result: NSObject, NSCoding {
    var label: String
    var hint: String
    var resultImage: UIImage
    
    init(label: String, hint: String, resultImage: UIImage) {
        self.label = label
        self.hint = hint
        self.resultImage = resultImage
    }
    
    override init() {
        self.label = ""
        self.hint = ""
        self.resultImage = #imageLiteral(resourceName: "Unlock_Tapped_Site")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.label = aDecoder.decodeObject(forKey: "label") as! String
        self.hint = aDecoder.decodeObject(forKey: "hint") as! String
        self.resultImage =  aDecoder.decodeObject(forKey: "image") as! UIImage
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(label, forKey: "label")
        aCoder.encode(hint, forKey: "hint")
        aCoder.encode(resultImage, forKey: "image")
    }
}
